#include <iostream>
#include <fstream>

using namespace std;

string startFilename = "File.txt";
string resultFilename = "Answer.txt";

int main() {
    ifstream inputFileStream(startFilename);
    if(!inputFileStream.is_open())
    {
        cout << "can not open file!";
        exit(EXIT_FAILURE);
    }

    int arrayLength = 0;
    int intBuffer = 0;

    while (inputFileStream >> intBuffer) ++arrayLength;
    inputFileStream.close();

    int* arrayFromFile = new int[arrayLength];

    inputFileStream.open(startFilename);
    if(!inputFileStream.is_open())
    {
        cout << "can not open file!";
        exit(EXIT_FAILURE);
    }

    int i = 0;
    while (inputFileStream >> arrayFromFile[i++]);
    inputFileStream.close();

    const int N = arrayLength;

    int* b = new int[arrayLength];
    int* c = new int[arrayLength];

    int m = 0, t = 0;
    for (int i = 0; i < N; ++i) {
        b[i] = 1;
        c[i] = -1;
        for (int j = i - 1; j >= 0; --j) {
            if (b[i] < b[j] + 1 && arrayFromFile[i] >= arrayFromFile[j]) {
                b[i] = b[j] + 1;
                c[i] = j;
            }
        }
        if (m < b[i]) {
            m = b[i];
            t = i;
        }
    }

    int* resultArray = new int[m];

    for (int i = m - 1; i >= 0; --i) {
        resultArray[i] = arrayFromFile[t];
        t = c[t];
    }

    ofstream outputFileStream(resultFilename);
    if(!outputFileStream.is_open())
    {
        cout << "can not open file!";
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < m; ++i) outputFileStream << resultArray[i] << " ";
    outputFileStream.close();
    return 0;
}
