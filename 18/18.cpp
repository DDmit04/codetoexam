﻿//Разработайте программу, обрабатывающую файл целых чисел.Определите с помощью подпрограммы, 
//сколько чисел в файле встречается по одному разу.
#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
using namespace std;

string inputFilename = "input.txt";

void SingleNumbers(int* const arr, const int length)
{
	int k = 0, count = 0;
	for(int i = 1; i <= length; i++)
	{
		k = 0;
		for(int j = i + 1; j <= length - 1; j++)
		{
			if(arr [i] == arr [j] && i != j)
			{
				break;
			}
			else if(arr [i] != arr [j] && i != j)
			{
				k++;
			}
		}
		if(k != 0)
		{
			count++;
		}
	}
	cout << endl << "Количество чисел, встечающихся по 1 разу: " << count;
}
void main()
{
	setlocale(0, "");
	ifstream inputFileStream(inputFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}
	int intBuffer;
	int arr [1000];
	int length = 0;
	while(inputFileStream >> intBuffer)
	{
		arr [length++] = intBuffer;
	}
	cout << "Искомый массив:" << endl;
	inputFileStream.close();
	SingleNumbers(arr, length);
}
