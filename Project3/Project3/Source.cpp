#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

const int matrixLength = 6;
string matrixMediumFilename = "F.txt";
string matrixMimMaxFilename = "G.txt";

struct Matrix
{
	string name;
	int matrixContent [matrixLength][matrixLength];
};

void InputMatrix(Matrix& matrix)
{
	cout << "Input Matrix " << matrix.name << "\n";

	for(int i = 0; i < matrixLength; ++i)
	{
		for(int j = 0; j < matrixLength; ++j)
		{
			cin >> matrix.matrixContent [i][j];
		}
	}
}

double Average(int elementsCount, Matrix matrix)
{
	int sum = 0;

	for(int i = 0; i < matrixLength; ++i)
	{
		for(int j = 0; j < matrixLength; ++j)
		{
			sum += matrix.matrixContent [i][j];
		}
	}

	return 1.0 * sum / elementsCount;
}

void InputInFileSum(Matrix matrix)
{
	ofstream matrixMediumFileStream(matrixMediumFilename, ios_base::app);

	if(!matrixMediumFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	matrixMediumFileStream << matrix.name << ": " << Average(matrixLength * matrixLength, matrix) << "\n";
	matrixMediumFileStream.close();
}

int main()
{
	//ofstream fileStream("F.txt");
	//fileStream.close();

	Matrix A = { "A" }, B = { "B" }, C = { "C" }, D = { "D" };
	InputMatrix(A);
	InputMatrix(B);
	InputMatrix(C);
	InputMatrix(D);

	InputInFileSum(A);
	InputInFileSum(B);
	InputInFileSum(C);
	InputInFileSum(D);

	int a = Average(matrixLength * matrixLength, A);
	int b = Average(matrixLength * matrixLength, B);
	int c = Average(matrixLength * matrixLength, C);
	int d = Average(matrixLength * matrixLength, D);

	int maximum = max(max(a, b), max(c, d));
	int minimum = min(min(a, b), min(c, d));

	ofstream minMaxFileStream(matrixMimMaxFilename);

	if(!minMaxFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	minMaxFileStream << "Max: " << maximum << "\n";
	minMaxFileStream << "Min: " << minimum << "\n";

	return 0;
}
