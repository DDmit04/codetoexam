#include <iostream>
#include <string>

using namespace std;

struct Abonent
{
	string name, number;
};

struct Queue
{
	struct Node
	{
		Abonent elem;
		Node* next;
	}*queue;

	Queue() { queue = NULL; }

	Abonent searchByName(string name)
	{
		Abonent searchedAbonent;
		Node* temp = queue;

		while(temp != NULL)
		{
			if(temp->elem.name == name)
			{
				searchedAbonent = temp->elem;
				break;
			}
			temp = temp->next;
		}
		return searchedAbonent;
	}

	void Enqueue(Abonent newAbonent)
	{
		Node* newNode = new Node;
		newNode->elem = newAbonent;
		newNode->next = NULL;
		if(queue == NULL)
		{
			queue = newNode;
			return;
		}

		Node* temp = queue;

		while(temp->next != NULL)
		{
			temp = temp->next;
		}

		temp->next = newNode;
	}

	Abonent Dequeue()
	{
		Node* temp = queue;
		Abonent n = queue->elem;
		queue = queue->next;
		delete temp;

		return n;
	}

	void Display()
	{
		Node* temp = queue;
		int k = 0;
		while(temp != NULL)
		{
			cout << ++k << ") " << temp->elem.name << "  -  " << temp->elem.number << "\n";
			temp = temp->next;
		}
	}
};

int main()
{
	int abonentsCount = 0;
	cout << "Input abonents count to add: ";
	cin >> abonentsCount;

	Queue currentQueue;
	cout << "Input data\n";
	while(abonentsCount--)
	{
		Abonent newAbonent;
		cin >> newAbonent.name >> newAbonent.number;
		currentQueue.Enqueue(newAbonent);
	}
	string abonentName = "";
	cout << "input abonent name to get it's number: " << endl;
	cin >> abonentName;
	Abonent searchedAbonent = currentQueue.searchByName(abonentName);
	if(searchedAbonent.name == abonentName)
	{
		cout << "searched abonent number: " << searchedAbonent.number << endl;
	}
	else
	{
		cout << "no abonent with this name!" << endl;
	}
	cout << "all abonents:" << endl;
	currentQueue.Display();
}
