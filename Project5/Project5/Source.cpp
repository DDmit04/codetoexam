#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <fstream>

using namespace std;

string arraysFilename = "File.txt";

struct Array
{

	int length = 1;
	int* arrayContent = new int [length];

	void BubbleSort()
	{
		for(int i = 0; i < length; ++i)
		{
			for(int j = 0; j < length - 1; ++j) if(arrayContent [j] > arrayContent [j + 1]) swap(arrayContent [j], arrayContent [j + 1]);
		}

	}
	void CreateArray(int arrLength)
	{
		delete arrayContent;
		length = arrLength;
		arrayContent = new int [arrLength];
	}

};

void stringToIntArray(Array& resultArray, string stringToConvert)
{
	string convertBuffer = "";
	int* tempArray = new int[stringToConvert.size()];

	int elementNumber = 0;
	int nextSpaceIndex = 0;
	while(stringToConvert != "")
	{
		nextSpaceIndex = stringToConvert.find_first_of(' ');
		if(nextSpaceIndex != -1)
		{
			convertBuffer = stringToConvert.substr(0, nextSpaceIndex);
			stringToConvert = stringToConvert.substr(nextSpaceIndex + 1);
		}
		else
		{
			convertBuffer = stringToConvert;
			stringToConvert = "";
		}
		tempArray [elementNumber] = atoi(convertBuffer.c_str());
		elementNumber++;
	}

	resultArray.CreateArray(elementNumber);
	for(int j = 0; j < elementNumber; j++)
	{
		resultArray.arrayContent[j] = tempArray [j];
	}
	delete[](tempArray);
}



int main()
{
	ifstream arraysInFileStream(arraysFilename);
	if(!arraysInFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	string buffer;
	int arraysCount = 0;

	while(getline(arraysInFileStream, buffer)) ++arraysCount;

	arraysInFileStream.close();

	arraysInFileStream.open(arraysFilename);
	if(!arraysInFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	Array* arrays = new Array [arraysCount];

	int i = 0;

	while(getline(arraysInFileStream, buffer))
	{
		stringToIntArray(arrays [i], buffer);
		arrays [i++].BubbleSort();
	}

	arraysInFileStream.close();


	ofstream arraysOutFileStream(arraysFilename);
	if(!arraysOutFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	for(int i = 0; i < arraysCount; ++i)
	{
		for(int j = 0; j < arrays [i].length - 1; ++j)
		{
			arraysOutFileStream << arrays [i].arrayContent [j] << " ";
		}
		arraysOutFileStream << arrays [i].arrayContent [arrays [i].length - 1] << "\n";
	}

	arraysOutFileStream.close();
	delete(arrays);
	return 0;
}
