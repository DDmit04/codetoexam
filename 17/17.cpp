﻿//Разработайте программу, обрабатывающую текстовый файл.В текстовом файле содержится строка.
//Удалите из строки все повторения.Новую строку перепишите в другой файл.
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

string inputFilename = "input.txt";
string outputFilename = "ouput.txt";

void main()
{
	setlocale(0, "");
	string buffer, resultString;

	ifstream inputFileStream(inputFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	while(!inputFileStream.eof())
	{
		getline(inputFileStream, buffer);
	}
	inputFileStream.close();
	for(int n = 0; n < buffer.size(); n++)
	{
		if(buffer [n] == buffer [n + 1])
		{
			buffer.erase(n, 1);
		}
	}
	resultString = buffer;
	for(int n = 0; n < buffer.size(); n++)
	{
		for(int m = n + 1; m < resultString.size() + 1; m++)
		{
			if(buffer [n] == resultString [m])
			{
				resultString.erase(m, 1);
			}
		}
	}
	ofstream outputFileStream(outputFilename);
	if(!outputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	outputFileStream << resultString;
	outputFileStream.close();
}
