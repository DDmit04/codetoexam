﻿//Дан файл массивов.Используя подпрограмму подсчета суммы элементов одномерного массива, 
//для всех массивов найдите максимальные минимальные суммы элементов.Все полученные результаты сохраните другом файле.

#include <fstream>
#include <iostream>
using namespace std;

string inputFilename = "input.txt";
string outputFilename = "output.txt";

void SumOfElements(double** arr, double* sum, int i, const int columns)
{
	sum [i] = 0.0;
	for(int j = 0; j < columns; j++)
	{
		sum [i] += arr [i][j];
	}
}
void main()
{
	setlocale(LC_ALL, "RUSSIAN");

	//Создаем файловый поток и связываем его с файлом
	ifstream inputFileStream(inputFilename);

	if(!inputFileStream.is_open())
	{
		cout << "can not open file";
		exit(EXIT_FAILURE);
	}

	//Если открытие файла прошло успешно
	//Вначале посчитаем сколько чисел в файле
	int count = 0;// число чисел в файле
	int temp;//Временная переменная

	while(!inputFileStream.eof())// пробегаем пока не встретим конец файла eof
	{
		inputFileStream >> temp;//в пустоту считываем из файла числа
		count++;// увеличиваем счетчик числа чисел
	}

	//Число чисел посчитано, теперь нам нужно понять сколько
	//чисел в одной строке
	//Для этого посчитаем число пробелов до знака перевода на новую строку

	//Вначале переведем каретку в потоке в начало файла
	inputFileStream.seekg(0, ios::beg);
	inputFileStream.clear();

	//Число пробелов в первой строчке вначале равно 0
	int count_space = 0;
	char symbol;
	while(!inputFileStream.eof())//на всякий случай цикл ограничиваем концом файла
	{
		//теперь нам нужно считывать не числа, а посимвольно считывать данные
		inputFileStream.get(symbol);//считали текущий символ
		if(symbol == ' ') count_space++;//Если это пробел, то число пробелов увеличиваем
		if(symbol == '\n') break;//Если дошли до конца строки, то выходим из цикла
	}
	//cout « count_space « endl;

	//Опять переходим в потоке в начало файла
	inputFileStream.seekg(0, ios::beg);
	inputFileStream.clear();

	//Теперь мы знаем сколько чисел в файле и сколько пробелов в первой строке.
	//Теперь можем считать матрицу.

	int lines = count / (count_space + 1);//число строк
	int columns = count_space + 1;//число столбцов на единицу больше числа пробелов
	double** arr;
	arr = new double* [lines];
	for(int i = 0; i < lines; i++) arr [i] = new double [columns];

	//Считаем матрицу из файла
	for(int i = 0; i < lines; i++)
		for(int j = 0; j < columns; j++)
			inputFileStream >> arr [i][j];

	inputFileStream.close();

	//нахождение максимальных и минимальных сумм элементов для всех массивов, используя подпрограмму подсчета суммы элементов одномерного массива
	double* sum, max, min;
	sum = new double [lines];
	for(int i = 0; i < lines; i++)
	{
		SumOfElements(arr, sum, i, columns);
		max = sum [0];
		min = sum [0];
	}
	for(int i = 0; i < lines; i++)
	{
		if(sum [i] > max)
		{
			max = sum [i];
		}
		if(sum [i] < min)
		{
			min = sum [i];
		}
	}
	ofstream outputFileStream(outputFilename);
	if(!outputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}
	outputFileStream << "Максимальная сумма элементов: " << max << endl << "Мимимальная сумма элементов: " << min;
	outputFileStream.close();
}
