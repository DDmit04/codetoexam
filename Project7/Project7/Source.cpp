#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

using namespace std;

const int subjectsCount = 3;

string studentsFilename = "FileOne.txt";
string sortedStudentsFilename = "FileTwo.txt";

struct Student
{
	string name, surname, patr;
	int studentMarks [subjectsCount];

	void Input()
	{
		cout << "Name: ";
		cin >> name;
		cout << "Surname: ";
		cin >> surname;
		cout << "Patr: ";
		cin >> patr;
		for(int i = 0; i < subjectsCount; ++i) cout << "Mark " << i + 1 << ": ", cin >> studentMarks [i];
	}

	bool isGood()
	{
		int marksSum = 0;
		for(int i = 0; i < subjectsCount; ++i) marksSum += studentMarks [i];

		if(1.0 * marksSum / subjectsCount >= 4.5) return true;
		return false;
	}

	void Output()
	{
		cout << name << " " << surname << " " << patr;
		for(int i = 0; i < subjectsCount; ++i) cout << " " << studentMarks [i];
		cout << "\n";
	}
};

void SwapRec(Student& firstStudent, Student& cesStudent)
{
	for(int i = 0; i < subjectsCount; ++i) if(firstStudent.studentMarks [i] > cesStudent.studentMarks [i])
	{
		swap(firstStudent, cesStudent);
		return;
	}
}

int main()
{
	int studentsCount;
	cout << "Input students count: ";
	cin >> studentsCount;

	Student* students = new Student [studentsCount];

	cout << "Input data\n";
	for(int i = 0; i < studentsCount; ++i)
	{
		students [i].Input();
	}

	ofstream fout(studentsFilename);
	for(int i = 0; i < studentsCount; ++i)
	{
		fout << students [i].name << " " << students [i].surname << " " << students [i].patr << " ";
		for(int j = 0; j < subjectsCount; ++j)
		{
			fout << students [i].studentMarks [j] << " ";
		}
		fout << "\n";
	}
	fout.close();

	for(int i = 0; i < studentsCount; ++i)
	{
		for(int j = 0; j < studentsCount - 1; ++j)
		{
			SwapRec(students [j], students [j + 1]);
		}
	}
	fout.open(sortedStudentsFilename);
	for(int i = 0; i < studentsCount; ++i)
	{
		fout << students [i].name << " " << students [i].surname << " " << students [i].patr << " ";
		for(int j = 0; j < subjectsCount; ++j)
		{
			fout << students [i].studentMarks [j] << " ";
		}
		fout << "\n";
	}
	fout.close();

	for(int i = 0; i < studentsCount; ++i) if(students [i].isGood()) students [i].Output();

	return 0;
}
