#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string startFilename = "File.txt";

struct Stack
{
	struct Node
	{
		int elem;
		Node* next;
	}*stack;

	Stack() { stack = NULL; }

	void push(int n)
	{
		Node* temp = new Node;

		temp->elem = n;
		temp->next = stack;

		stack = temp;
	}

	int pop()
	{
		if(stack == NULL)
		{
			return 0;
		}
		Node* temp = stack;

		int ans = temp->elem;

		stack = stack->next;

		return ans;
	}

	void display()
	{
		Node* temp = stack;

		while(temp->next != NULL)
		{
			cout << "[" << temp->elem << "] <- ";
			temp = temp->next;
		}

		cout << "[" << temp->elem << "]\n";
	}
};

int main()
{
	ifstream inputFileStream(startFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	Stack currentStack;

	int intBuffer;
	while(inputFileStream >> intBuffer)
	{
		if(intBuffer % 5 == 0)
		{
			currentStack.push(0);
		}
		currentStack.push(intBuffer);
	}

	currentStack.display();
	return 0;
}
