#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string dijitFilename = "FIle.txt";

struct Stack
{
	struct Node
	{
		int elem;
		Node* next;
	}*stack;

	Stack() { stack = NULL; }

	void push(int n)
	{
		Node* temp = new Node;
		temp->elem = n;
		temp->next = stack;
		stack = temp;
		return;
	}

	void pop()
	{
		Node* temp = stack;
		stack = stack->next;
		delete temp;
	}

	double Average()
	{
		int index = 0;
		int sum = 0;
		int elenentsCount = 0;
		Node* temp = stack;
		while(temp != NULL)
		{
			if(index % 2 == 0)
			{
				++elenentsCount;
				sum += temp->elem;
			}
			index++;
			temp = temp->next;
		}

		return 1.0 * sum / elenentsCount;
	}

	void Display()
	{
		Node* temp = stack;
		while(temp->next != NULL)
		{
			cout << "[" << temp->elem << "] <- ";
			temp = temp->next;
		}
		cout << "[" << temp->elem << "]\n";
	}
};

int main()
{
	Stack currentStack;
	ifstream dijitFileStream(dijitFilename);
	if(!dijitFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	int nextDijit;
	while(dijitFileStream >> nextDijit) currentStack.push(nextDijit);

	dijitFileStream.close();

	currentStack.Display();
	cout << "Middle of stack: " << currentStack.Average();
	return 0;
}
