﻿//В текстовом файле найдите и удалите все слова, начинающиеся и заканчивающиеся одной и той же буквой.
#include <iostream>
#include <string>
#include <fstream>
#include <sstream> 
using namespace std;

string inputFileName = "input.txt";

void main()
{
    setlocale(0, "");
    string wordCount, buffer, resultString, singleWord;

    ifstream inputFileStream(inputFileName);
    if(!inputFileStream.is_open())
    {
        cout << "can not open file!";
        exit(EXIT_FAILURE);
    }

    while(!inputFileStream.eof())
    {
        getline(inputFileStream, buffer);
        wordCount += buffer;
        if(!inputFileStream.eof())
        {
            wordCount += '\n';
        }
    }
    inputFileStream.close();

    //форматирование строки - разбиение её на слова, своеобразная инициализация stringstream
    stringstream words(wordCount);
    //передача по 1 слову в поток для обработки
    while(words >> singleWord)
    {
        //если после слова стоит какой-то знак препинания и слово начинается и
        //заканчивается одной и той же буквой, то удалить это слово
        if((singleWord [singleWord.size() - 1] == '.') || (singleWord [singleWord.size() - 1] == ',') || (singleWord [singleWord.size() - 1] == ':') || (singleWord [singleWord.size() - 1] == ';') || (singleWord [singleWord.size() - 1] == '-') || (singleWord [singleWord.size() - 1] == '?') || (singleWord [singleWord.size() - 1] == '!'))
        {
            if(((singleWord [0] != singleWord [singleWord.size() - 2]) && (singleWord.size() != 1)) || (singleWord.size() == 1))
            {
                resultString += singleWord + " ";
            }
        }
        else
            //если слово начинается и заканчивается одной и той же буквой, то удалить это слово
            if(((singleWord [0] != singleWord [singleWord.size() - 1]) && (singleWord.size() != 1)) || (singleWord.size() == 1))
            {
                resultString += singleWord + " ";
            }
    }
    //удаление из строки последовательности символов заданной длины
    wordCount = resultString.erase(resultString.size() - 1);
    cout << wordCount;
}
