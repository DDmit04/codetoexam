#include <iostream>
#include <fstream>

using namespace std;

string startTextFilename = "FileOne.txt";
string resultTextFilename = "FileTwo.txt";

int main() {
    ifstream inputFileStream(startTextFilename);
    ofstream outputFileStream(resultTextFilename);

    if(!outputFileStream.is_open() || !inputFileStream.is_open())
    {
        cout << "can not open file!";
        exit(EXIT_FAILURE);
    }

    string buffer;
    while (inputFileStream >> buffer) {
        int i = buffer.size() - 1;

        if ((char)tolower(buffer[0]) == (char) tolower(buffer[i]) && buffer.size() > 1) 
            outputFileStream << buffer << " ";
    }

    inputFileStream.close();
    outputFileStream.close();
    return 0;
}
