#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string workFile = "File.txt";

struct Stack
{
	struct Node
	{
		string elem;
		Node* next;
	}*stack;

	Stack() { stack = NULL; }

	void push(string s)
	{
		Node* temp = new Node;

		temp->elem = s;
		temp->next = stack;

		stack = temp;
	}

	string pop()
	{
		Node* temp = stack;
		stack = stack->next;

		string elemValue;
		elemValue = temp->elem;

		delete temp;
		return elemValue;
	}

	void display()
	{
		int elemIndex = 0;
		Node* temp = stack;

		while(temp != NULL)
		{
			cout << ++elemIndex << ") " << temp->elem << "\n";
			temp = temp->next;
		}
	}
};

int main()
{
	ifstream inputFileStream(workFile);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}
	string buffer, longestWord;
	Stack currentStack;
	while(inputFileStream >> buffer)
	{
		currentStack.push(buffer);
		if(longestWord.size() < buffer.size()) longestWord = buffer;
	}
	inputFileStream.close();

	currentStack.display();

	cout << "longest word in stack: " << longestWord;
	return 0;
}
