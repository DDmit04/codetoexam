﻿//Разработайте программу, обрабатывающую файл целочисленных массивов.
//С помощью подпрограммы нахождения среднего арифметического значения среди элементов одномерного массива, 
//запишите в другой файл среднее арифметическое каждого массива исходного файла.
#include <fstream>
#include <iostream>
using namespace std;

string inputFilename = "input.txt";
string outputFilename = "ouput.txt";

void ArithmeticalMean(int** arr, double* average, int* sum, int i, const int columns)
{
	sum [i] = 0.0;
	for(int j = 0; j < columns; j++)
	{
		sum [i] += arr [i][j];
	}
	average [i] = (double) sum [i] / (double) columns;
}
void main()
{
	setlocale(LC_ALL, "RUSSIAN");

	//Создаем файловый поток и связываем его с файлом
	ifstream inputFileStream(inputFilename);

	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	//Если открытие файла прошло успешно
	//Вначале посчитаем сколько чисел в файле
	int count = 0;// число чисел в файле
	int temp;//Временная переменная

	while(!inputFileStream.eof())// пробегаем пока не встретим конец файла eof
	{
		inputFileStream >> temp;//в пустоту считываем из файла числа
		count++;// увеличиваем счетчик числа чисел
	}

	//Число чисел посчитано, теперь нам нужно понять сколько
	//чисел в одной строке
	//Для этого посчитаем число пробелов до знака перевода на новую строку

	//Вначале переведем каретку в потоке в начало файла
	inputFileStream.seekg(0, ios::beg);
	inputFileStream.clear();

	//Число пробелов в первой строчке вначале равно 0
	int count_space = 0;
	char symbol;
	while(!inputFileStream.eof())//на всякий случай цикл ограничиваем концом файла
	{
		//теперь нам нужно считывать не числа, а посимвольно считывать данные
		inputFileStream.get(symbol);//считали текущий символ
		if(symbol == ' ') count_space++;//Если это пробел, то число пробелов увеличиваем
		if(symbol == '\n') break;//Если дошли до конца строки, то выходим из цикла
	}
	//cout « count_space « endl;

	//Опять переходим в потоке в начало файла
	inputFileStream.seekg(0, ios::beg);
	inputFileStream.clear();

	//Теперь мы знаем сколько чисел в файле и сколько пробелов в первой строке.
	//Теперь можем считать матрицу.

	int lines = count / (count_space + 1);//число строк
	int columns = count_space + 1;//число столбцов на единицу больше числа пробелов
	int** arr;
	arr = new int* [lines];
	for(int i = 0; i < lines; i++) arr [i] = new int [columns];

	//Считаем матрицу из файла
	for(int i = 0; i < lines; i++)
		for(int j = 0; j < columns; j++)
			inputFileStream >> arr [i][j];

	//нахождение среднего арифметического значения среди элементов одномерного массива
	int* sum, max, min;
	double* average;
	average = new double [lines];
	sum = new int [lines];
	for(int i = 0; i < lines; i++)
	{
		ArithmeticalMean(arr, average, sum, i, columns);
	}
	ofstream outpputFileStream(outputFilename);
	for(int i = 0; i < lines; i++)
	{
		outpputFileStream << "Среднее арифметическое элементов " << i + 1 << " одномерного массива = " << average [i] << endl;
	}
	outpputFileStream.close();
}
