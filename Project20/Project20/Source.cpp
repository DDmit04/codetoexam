#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string numbersFilename = "File.txt";

struct Queue
{
	struct Node
	{
		int elem;
		Node* next;
	}*queue;

	Queue() { queue = NULL; }

	void Enqueue(int n)
	{
		Node* temp = new Node;

		temp->elem = n;
		temp->next = queue;

		queue = temp;
	}

	int Dequeue()
	{
		Node* temp = queue;
		if(queue->next == NULL)
		{
			delete temp;
			queue = NULL;
			return 0;
		}
		while(temp->next->next != NULL) temp = temp->next;

		int ans = temp->elem;
		delete temp;
		return ans;
	}

	void Display()
	{
		Node* temp = queue;

		while(temp->next != NULL)
		{
			cout << "[" << temp->elem << "] -> ";
			temp = temp->next;
		}
		cout << "[" << temp->elem << "]\n";
	}
};

int main()
{
	ifstream inputFileStream(numbersFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	int intBuffer = 0;
	int fileArrayLength = 0;

	while(inputFileStream >> intBuffer) ++fileArrayLength;

	inputFileStream.close();

	inputFileStream.open(numbersFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	Queue currentQueue;
	int* arrayFromFile = new int [fileArrayLength];
	int i = 0;

	while(inputFileStream >> arrayFromFile [i])
	{
		currentQueue.Enqueue(arrayFromFile [i++]);
	}

	inputFileStream.close();

	int sequenceLength = 1;
	for(int i = 1; i < fileArrayLength; ++i)
	{
		if(arrayFromFile [i] == arrayFromFile [i - 1]) ++sequenceLength;
		else if(sequenceLength >= 2)
		{
			currentQueue.Enqueue(sequenceLength);
			sequenceLength = 1;
		}
	}

	currentQueue.Display();
	return 0;
}
