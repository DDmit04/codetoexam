#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>

using namespace std;

const int matrixLength = 5;

struct Matrix {
    int matrixContent[matrixLength][matrixLength];
};

void InputRandom(Matrix& matrix) {

    for (int i = 0; i < matrixLength; ++i) {
        for (int j = 0; j < matrixLength; ++j) matrix.matrixContent[i][j] = (rand() % 23) - 10;
    }
}

void OutputMatrix(Matrix A) {
    for (int i = 0; i < matrixLength; ++i) {
        for (int j = 0; j < matrixLength; ++j) cout << setw(3) << A.matrixContent[i][j];
        cout << "\n";
    }
}

int Sum(Matrix matrix) {
    int sum = 0;
    for (int i = 0; i < matrixLength; ++i) {
        for (int j = 0; j < matrixLength; ++j) {
            if (matrix.matrixContent[i][j] > 0) sum += matrix.matrixContent[i][j];
        }
    }

    return sum;
}

void InputFile(string filename, Matrix matrix) {

    ofstream outputFileStream(filename, ios_base::app | ios_base::binary);

    if(!outputFileStream.is_open())
    {
        cout << "can not open file!";
        exit(EXIT_FAILURE);
    }
    else
    {
        outputFileStream.write((char*) &matrix, sizeof matrix);
        outputFileStream.close();
    }
}

int main() {

    srand(time(0));
    Matrix A, B, C;
    InputRandom(A);
    InputRandom(B);
    InputRandom(C);

    cout << "Matrix A\n";
    OutputMatrix(A);
    cout << "Matrix B\n";
    OutputMatrix(A);
    cout << "Matrix C\n";
    OutputMatrix(A);

    cout << "Sum A: " << Sum(A) << "\n";
    cout << "Sum B: " << Sum(B) << "\n";
    cout << "Sum C: " << Sum(C) << "\n";

    cout << "Input name file: ";
    string name;
    cin >> name;

    InputFile(name, A);
    InputFile(name, B);
    InputFile(name, C);

    return 0;
}
