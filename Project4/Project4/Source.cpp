#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class Queue
{
public:
	struct Node
	{
		char value;
		Node* next;
	}*queue;

	Queue() { queue = NULL; }

	void Enqueue(char newElementValue)
	{
		Node* newNode = new Node;
		newNode->value = newElementValue;
		newNode->next = queue;
		queue = newNode;
	}

	void Dequeue()
	{
		Node* temp = queue;
		if(temp == NULL)
		{
			cout << "Queue is empty!\n";
			return;
		}

		if(temp->next == NULL)
		{
			queue = NULL;
			delete temp;
			return;
		}

		while(temp->next->next != NULL) temp = temp->next;

		delete temp->next;
		temp->next = NULL;

		return;
	}

	void Display()
	{
		Node* temp = queue;

		if(temp == NULL)
		{
			cout << "Queue is empty!\n";
			return;
		}

		while(temp->next != NULL)
		{
			cout << "[" << temp->value << "] -> ";
			temp = temp->next;
		}
		cout << "[" << temp->value << "]\n";
	}

	bool isContainDigit()
	{
		Node* temp = queue;
		while(temp != NULL)
		{
			if(temp->value >= '0' && temp->value <= '9') return true;
			temp = temp->next;
		}

		return false;
	}
	void removeAllDijits()
	{
		while(isdigit(queue->value))
		{
			queue = queue->next;
		}
		Node* temp = queue;
		Node* prevNode = queue;
		while(temp != NULL)
		{
			if(isdigit(temp->value))
			{
				prevNode->next = temp->next;
				temp = temp->next;
			}
			else
			{
				prevNode = temp;
				temp = temp->next;
			}
		}
	}
};

void RandomQueue(Queue& queueToFill)
{
	int queueLength = (rand() % 13) + 1;
	while(queueLength--)
	{
		int value = (rand() % 62);
		if(value < 10) queueToFill.Enqueue(value + '0');
		else if(value < 37) queueToFill.Enqueue(value - 10 + 'A');
		else queueToFill.Enqueue(value - 37 + 'a');
	}
}

int main()
{
	srand(time(0));
	Queue currentQueue;
	cout << "Queue: ";
	RandomQueue(currentQueue);
	currentQueue.Display();

	cout << "Queue without Digit: ";
	currentQueue.removeAllDijits();
	currentQueue.Display();
	return 0;
}
