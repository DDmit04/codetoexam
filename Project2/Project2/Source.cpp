#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string startTextFilename = "File.txt";
string alfsFilename = "Text.txt";
string sumFilename = "SumDigit.txt";

int main()
{

	ifstream startTextStream(startTextFilename);
	ofstream alfsFileStream(alfsFilename);
	ofstream sumFileStream(sumFilename);

	if(!startTextStream.is_open() || !alfsFileStream.is_open() || !sumFileStream.is_open())
	{
		cout << "can not open files!";
		exit(EXIT_FAILURE);
	}

	int sum = 0;
	string textBuffer = "";

	while(getline(startTextStream, textBuffer))
	{
		for(auto stringChar : textBuffer)
		{
			if(stringChar >= '0' && stringChar <= '9')
			{
				sum += stringChar - '0';
				continue;
			}

			switch((char) tolower(stringChar))
			{
				case 'a': case 'e':
				case 'i': case 'o':
				case 'u': case 'y':
					alfsFileStream << stringChar << " ";
			}
		}
	}
	sumFileStream << sum;

	startTextStream.close();
	alfsFileStream.close();
	sumFileStream.close();

	return 0;
}
