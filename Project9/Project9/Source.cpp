#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string arrayFilename = "File.txt";

struct Queue
{
	struct Node
	{
		char elem;
		Node* next;
	}*queue;

	Queue() { queue = NULL; }

	void Enqueue(char c)
	{
		Node* temp = new Node;

		temp->elem = c;
		temp->next = queue;
		queue = temp;
	}

	void Dequeue()
	{
		Node* temp = queue;

		if(temp->next == NULL)
		{
			delete temp;
			queue = NULL;
		}

		while(temp->next->next != NULL) temp = temp->next;
		delete temp->next;
		temp->next = NULL;
	}

	void Display()
	{

		Node* temp = queue;
		while(temp->next != NULL) cout << "[" << temp->elem << "] -> ", temp = temp->next;
		cout << "[" << temp->elem << "]\n";

	}

	bool isVowel(Node* node)
	{
		switch((char) tolower(node->elem))
		{
			case 'a': case 'e': case 'i':
			case 'o': case 'u': case 'y':
				return true;
		}
		return false;
	}
	void removeAllVowels()
	{
		while(isVowel(queue))
		{
			queue = queue->next;
		}
		Node* temp = queue;
		Node* prevNode = queue;
		while(temp != NULL)
		{
			if(isVowel(temp))
			{
				prevNode->next = temp->next;
				temp = temp->next;
			}
			else
			{
				prevNode = temp;
				temp = temp->next;
			}
		}
	}
};



int main()
{

	ifstream inputFileStream(arrayFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open files!";
		exit(EXIT_FAILURE);
	}

	Queue currentQueue;
	string buffer = "";

	while(inputFileStream >> buffer)
	{
		for(auto bufferChar : buffer)
		{
			if((bufferChar >= 'a' && bufferChar <= 'z') || (bufferChar >= 'A' && bufferChar <= 'Z')) currentQueue.Enqueue(bufferChar);
		}
	}
	inputFileStream.close();

	currentQueue.Display();

	currentQueue.removeAllVowels();
	cout << "Answer: ";

	currentQueue.Display();
	return 0;
}
