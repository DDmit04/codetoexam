#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

using namespace std;

string arrayFilename = "File.txt";
string minMaxFilename = "FileAns.txt";

int getMax(int* array, int arrayLength)
{
	int maxElem = array [0];
	for(int i = 0; i < arrayLength; ++i) maxElem = max(maxElem, array [i]);
	return maxElem;
}


int getMin(int* array, int arrayLength)
{
	int minElem = array [0];
	for(int i = 0; i < arrayLength; ++i) minElem = min(minElem, array [i]);
	return minElem;
}

int main()
{
	int arrayLength = 1;

	ifstream inputFileStream(arrayFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}


	string buffer = "";
	int bufferSize = 0;

	getline(inputFileStream, buffer);
	bufferSize = buffer.size();

	for(int i = 0; i < bufferSize; i++)
	{
		if(buffer [i] == ' ' && buffer [i + 1] != ' ' && buffer [i + 1] && i) ++arrayLength;
	}
	inputFileStream.close();

	inputFileStream.open(arrayFilename);
	if(!inputFileStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}

	int* arrayFromFile = new int [arrayLength];

	int i = 0;

	while(inputFileStream >> arrayFromFile [i++]);
	inputFileStream.close();

	ofstream fileOutputStream(minMaxFilename);
	if(!fileOutputStream.is_open())
	{
		cout << "can not open file!";
		exit(EXIT_FAILURE);
	}
	fileOutputStream << "Max: " << getMax(arrayFromFile, arrayLength) << "\n";
	fileOutputStream << "Min: " << getMin(arrayFromFile, arrayLength);
	fileOutputStream.close();

	return 0;
}
